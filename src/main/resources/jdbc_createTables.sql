CREATE SEQUENCE pessoa_seq;
CREATE SEQUENCE endereco_seq;

CREATE TABLE pessoa
(
  id INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('pessoa_seq'),
  nome character varying(30) NOT NULL,
  email character varying(50) 
) 
WITHOUT OIDS;
ALTER TABLE pessoa OWNER TO postgres;

CREATE TABLE endereco
(
  codigo INTEGER NOT NULL PRIMARY KEY DEFAULT nextval('endereco_seq'),
  logradouro character varying(70) NOT NULL,
  bairro character varying(50),
  cidade character varying(50),
 codigo_pessoa integer, FOREIGN KEY (codigo_pessoa)   REFERENCES pessoa (id)
)
WITHOUT OIDS;
ALTER TABLE endereco OWNER TO postgres;


CREATE TABLE pessoa
(
 id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
  nome character varying(30) NOT NULL,
  email character varying(50),
CONSTRAINT primary_keyPessoa PRIMARY KEY (id) 

); 